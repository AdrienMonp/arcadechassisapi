'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');


/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    async findOne(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.services['chassis-item'].findOne({ url: id });
        return sanitizeEntity(entity, { model: strapi.models['chassis-item'] });
    },

    async update(ctx) {
        const { id } = ctx.params;

        let entity;
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity = await strapi.services['chassis-item'].update({ url: id }, data, {
                files,
            });
        } else {
            entity = await strapi.services['chassis-item'].update({ url: id }, ctx.request.body);
        }

        return sanitizeEntity(entity, { model: strapi.models['chassis-item'] });
    },

    async addcapa(ctx) {
        // get id param
        const { id } = ctx.params;
        // get chassis with id
        let entity = await strapi.services['chassis-item'].findOne({ url: id });
        // copy capkit list
        let capaList = [...entity.capkit];
        // push into it the new capa
        capaList.push(ctx.request.body);
        // set an object to update the chassis
        const updatedEntity = { capkit: capaList };

        // update chassis
        entity = await strapi.services['chassis-item'].update({ url: id }, updatedEntity);
        // get created capa
        const newCapaList = [...entity.capkit];
        const newCapa = newCapaList[newCapaList.length - 1];
        // return created capa
        return sanitizeEntity(newCapa, { model: strapi.models['chassis-item'] });
    },

    async updatecapa(ctx) {
        // get id param
        const { id, capaid } = ctx.params;
        // get chassis with id
        let entity = await strapi.services['chassis-item'].findOne({ url: id });
        // copy capkit list
        let capaList = [...entity.capkit];
        // find index of capa
        const index = capaList.findIndex(item => item.id === parseInt(capaid));
        // update capa
        capaList[index].sprite = ctx.request.body.sprite;
        // save updated capa
        const updatedCapa = capaList[index];
        // set the object to update the chassis
        const updatedEntity = { capkit: capaList };

        // update chassis
        entity = await strapi.services['chassis-item'].update({ url: id }, updatedEntity);
        // return updated capa
        return sanitizeEntity(updatedCapa, { model: strapi.models['chassis-item'] });
    },

    async deletecapa(ctx) {
        // get id param
        const { id, capaid } = ctx.params;
        // get chassis with id
        let entity = await strapi.services['chassis-item'].findOne({ url: id });
        // copy capkit list
        let capaList = [...entity.capkit];
        // find index on capa
        const index = capaList.findIndex(item => item.id === parseInt(capaid));
        // save capa to delete
        const deletedCapa = capaList[index];
        // remove it from list
        capaList.splice(index, 1);
        // set the object to update the chassis
        const updatedEntity = { capkit: capaList };

        // update chassis
        entity = await strapi.services['chassis-item'].update({ url: id }, updatedEntity);
        // return deleted capa
        return sanitizeEntity(deletedCapa, { model: strapi.models['chassis-item'] });
    }
};
